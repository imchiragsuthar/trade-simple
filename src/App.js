import "./App.css";
import { ThemeProvider, createTheme } from "@mui/material";
import Login from "./components/Login";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AppContainer from "./container/AppContainer";

const theme = createTheme();

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/app/*" element={<AppContainer />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
