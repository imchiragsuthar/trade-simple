import { Grid, Box, Card, CardContent, Typography } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import React, { useMemo } from 'react';
import { masterAccCols, userAccCols } from './constants';

const AccountCard = ({ title, children }) => (
    <Grid style={{ width: '100%' }} item>
        <Box>
            <Card>
                <CardContent>
                    <Typography
                        variant='h5'
                        style={{ marginBottom: 20 }}
                    >
                        {title}
                    </Typography>
                    {children}
                </CardContent>
            </Card>
        </Box>
    </Grid>
);

const UserList = () => {

    const userCols = useMemo(() => [...userAccCols, { field: 'action', headerName: 'Action' }], [])
    const masterCols = useMemo(() => [...masterAccCols, { field: 'action', headerName: 'Action' }], [])

    return (
        <Grid rowSpacing={5} style={{ padding: 20 }} container>
            <AccountCard title="Master Account">
                <DataGrid
                    style={{ height: 160 }}
                    rows={[]}
                    pageSize={5}
                    columns={masterCols}
                />
            </AccountCard>
            <AccountCard title="User Account" >
                <DataGrid
                    style={{ height: 300 }}
                    rows={[]}
                    columns={userCols}
                    pageSize={5}

                />
            </AccountCard>
        </Grid >
    );
};

export default UserList;