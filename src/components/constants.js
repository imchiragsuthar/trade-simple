export const masterAccCols = [
  { field: "accName", headerName: "Account Name", width: 200 },
  { field: "platform", headerName: "Platform" },
  { field: "userName", headerName: "User Name" },
];

export const userAccCols = [
  { field: "accName", headerName: "Account Name", width: 200 },
  { field: "platform", headerName: "Platform" },
  { field: "userName", headerName: "User Name" },
  { field: "password", headerName: "Password" },
];
