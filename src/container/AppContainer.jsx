import React from 'react';
import Header from './Header';
import { Route, Routes } from 'react-router';
import UserList from '../components/UserList';
import { Container } from '@mui/material';

const AppContainer = () => {
    return (
        <>
            <Header />
            <Container maxWidth="xl">
                <Routes>
                    <Route path="/list" element={<UserList />} />
                </Routes>
            </Container>
        </>
    );
};

export default AppContainer;